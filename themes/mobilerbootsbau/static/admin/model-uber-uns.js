import title from './field-title.js'
import slug from './field-slug.js'
import body from './field-body.js'
import layout from './field-layout.js'
import showInMenu from './field-show-in-menu.js'

const uberUns = {
    format: 'toml-frontmatter',
    name: 'Uber uns',
    label: 'Uber Uns',
    label_singular: 'Uber uns',
    create: true,
    folder: 'content/uber-uns',
    editor: {
	preview: false
    },
    fields: [
	title,
	slug,
	layout,
	showInMenu,
	body
    ]
}

export default uberUns
