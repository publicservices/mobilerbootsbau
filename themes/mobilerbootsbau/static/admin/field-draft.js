export default {
    label: 'Draft',
    name: 'draft',
    widget: 'boolean',
    required: false,
    default: 'false',
    hint: 'Is this article a draft (work in progress)? If yes, it will not be published on the live website.'
}
