import title from './field-title.js'
import slug from './field-slug.js'
import draft from './field-draft.js'
import body from './field-body.js'
import index from './field-index.js'

const prozessablauf  = {
    format: 'toml-frontmatter',
    name: 'Prozessablaufs',
    label: 'prozessablauf',
    label_singular: 'Prozessablauf',
    folder: 'content/prozessablauf',
    create: true,
    slug: '{{title}}',
    editor: {
	preview: false
    },
    fields: [
	title,
	slug,
	draft,
	index,
	body
    ]
}

export default prozessablauf
