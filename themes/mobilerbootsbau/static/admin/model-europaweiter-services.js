import title from './field-title.js'
import slug from './field-slug.js'
import draft from './field-draft.js'
import body from './field-body.js'
import featuredImage from './field-featured-image.js'
import logoBrand from './field-logo.js'

const europaweiterServices  = {
    format: 'toml-frontmatter',
    name: 'Europaweiter Services',
    label: 'Europaweiter Services',
    label_singular: 'Europaweiter Service',
    folder: 'content/europaweiter-service',
    create: true,
    slug: '{{title}}',
    editor: {
	preview: false
    },
    fields: [
	title,
	slug,
	draft,
	featuredImage,
	logoBrand,
	body
    ]
}

export default europaweiterServices
