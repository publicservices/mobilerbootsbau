import gallery from './field-gallery.js'
import title from './field-title.js'
import body from './field-body.js'

const specialPages = {
    name: 'pages',
    label: 'Pages',
    editor: {
	preview: false
    },
    files: [
	{
	    label: 'Homepage',
	    name: 'homepage',
	    file: 'data/homepage.yml',
	    fields: [
		gallery,
		title,
		body
	    ]
	},
	{
	    label: 'Über uns',
	    name: 'uber-uns',
	    file: 'data/uberuns.yml',
	    fields: [
		gallery,
		title,
		body
	    ]
	},
	{
	    label: 'Kontakt',
	    name: 'kontakt',
	    file: 'data/kontakt.yml',
	    fields: [
		{
		    label: 'Bootsbaumeisterbetrieb',
		    name: 'bootsbaumeisterbetrieb',
		    widget: 'string',
		    hint: ''
		},
		{
		    label: 'Company name',
		    name: 'company_name',
		    widget: 'string',
		    hint: ''
		},
		{
		    label: 'Address Street Name',
		    name: 'address_street_name',
		    widget: 'string',
		    hint: ''
		},
		{
		    label: 'Address Street Number',
		    name: 'address_street_number',
		    widget: 'string',
		    hint: ''
		},
		{
		    label: 'Address City',
		    name: 'address_city',
		    widget: 'string',
		    hint: ''
		},
		{
		    label: 'Address Postal Code',
		    name: 'address_postal_code',
		    widget: 'string',
		    hint: ''
		},
		{
		    label: 'Address Land',
		    name: 'address_land',
		    widget: 'string',
		    hint: ''
		},
		{
		    label: 'Company website',
		    name: 'company_website',
		    widget: 'string',
		    hint: ''
		},
		{
		    label: 'Company email',
		    name: 'company_email',
		    widget: 'string',
		    hint: ''
		},
		{
		    label: 'Company phone',
		    name: 'company_phone',
		    widget: 'string',
		    hint: ''
		},
		
	    ]
	}
	
    ]
}

export default specialPages
