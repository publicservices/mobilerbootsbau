import title from './field-title.js'
import slug from './field-slug.js'
import draft from './field-draft.js'
import body from './field-body.js'
import featuredImage from './field-featured-image.js'
import gallery from './field-gallery.js'

const technikFurDieLangfahrtyacht  = {
    format: 'toml-frontmatter',
    name: 'Langfahrtyacht',
    label: 'Langfahrtyacht',
    label_singular: 'Langfahrtyacht',
    folder: 'content/technik-fur-die-langfahrtyacht',
    create: true,
    slug: '{{title}}',
    editor: {
	preview: false
    },
    fields: [
	title,
	slug,
	draft,
	featuredImage,
	gallery,
	body
    ]
}

export default technikFurDieLangfahrtyacht
