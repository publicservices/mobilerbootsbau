import collections from './collections.js'

export default {
    config: {
	// Skips config.yml.
	// By not skipping, the configs will be merged, with the js-version taking priority.
	load_config_file: false,
	display_url: window.location.origin,

	backend: {
	    name: 'gitlab',
	    branch: 'master',
	    repo: 'publicservices/mobilerbootsbau',
	    auth_type: 'implicit',
	    app_id: '39b4e938b086201c1fec999c500e1ec50856282f4ac54ebe169342635fde42f3'
	},

	media_folder: 'static/media/uploads',
	public_folder: 'media/uploads',

	collections
    }
}
