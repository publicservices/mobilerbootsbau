export default {
    label: 'index',
    name: 'number',
    widget: 'number',
    hint: 'Select the index, order of appearence of this item (from 1 to 999)'
}
