import title from './field-title.js'
import slug from './field-slug.js'
import draft from './field-draft.js'
import body from './field-body.js'
import featuredImage from './field-featured-image.js'
import gallery from './field-gallery.js'
import index from './field-index.js'

const montage  = {
    format: 'toml-frontmatter',
    name: 'Montage',
    label: 'Montage',
    label_singular: 'Montage',
    folder: 'content/montage',
    create: true,
    slug: '{{title}}',
    editor: {
	preview: false
    },
    fields: [
	title,
	slug,
	draft,
	index,
	featuredImage,
	gallery,
	body
    ]
}

export default montage
