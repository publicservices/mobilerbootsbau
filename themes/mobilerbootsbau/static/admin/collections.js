import vorteile from './model-vorteile.js'
import technikFurDieLangfahrtyacht from './model-technik-fur-die-langfahrtyacht.js'
import teakdecks from './model-teakdecks.js'
import selbstmontage from './model-selbstmontage.js'
import montage from './model-montage.js'
import interieurWohnraum from './model-interieur-wohnraum.js'
import herstellungsprozess from './model-herstellungsprozess.js'
import prozessaublauf from './model-prozessaublauf.js'
import europaweiterServices from './model-europaweiter-services.js'
import configs from './model-configs.js'
import specialPages from './model-pages.js'
/* import uberUns from './model-uber-uns.js' */

export default [
    teakdecks,
    selbstmontage,
    interieurWohnraum,
    europaweiterServices,
    technikFurDieLangfahrtyacht,
    herstellungsprozess,
    prozessaublauf,
    montage,
    vorteile,
    specialPages,
    configs,
]
