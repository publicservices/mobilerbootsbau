import title from './field-title.js'
import slug from './field-slug.js'
import draft from './field-draft.js'
import body from './field-body.js'
import index from './field-index.js'

const vorteile  = {
    format: 'toml-frontmatter',
    name: 'Vorteile',
    label: 'Vorteile',
    label_singular: 'Vorteile',
    folder: 'content/vorteile',
    create: true,
    slug: '{{title}}',
    editor: {
	preview: false
    },
    fields: [
	title,
	slug,
	draft,
	index,
	body
    ]
}

export default vorteile
