import title from './field-title.js'
import slug from './field-slug.js'
import draft from './field-draft.js'
import body from './field-body.js'
import featuredImage from './field-featured-image.js'
import gallery from './field-gallery.js'

const interieurWohnraum = {
    format: 'toml-frontmatter',
    name: 'interieur wohnraum',
    label: 'Interieur Wohnraum',
    label_singular: 'Interieur Wohnraum',
    folder: 'content/interieur-wohnraum',
    create: true,
    slug: '{{title}}',
    editor: {
	preview: false
    },
    fields: [
	title,
	slug,
	draft,
	featuredImage,
	gallery,
	body
    ]
}

export default interieurWohnraum

