export default {
    label: 'Brand logo',
    name: 'Logo',
    widget: 'image',
    required: false,
    hint: 'Select a logo, image for this item.'
}
