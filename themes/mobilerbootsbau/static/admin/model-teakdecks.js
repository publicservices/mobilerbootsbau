import title from './field-title.js'
import slug from './field-slug.js'
import draft from './field-draft.js'
import body from './field-body.js'
import featuredImage from './field-featured-image.js'
import gallery from './field-gallery.js'

const teakdecks  = {
    format: 'toml-frontmatter',
    name: 'Teakdecks',
    label: 'Teakdecks',
    label_singular: 'Teakdeck',
    folder: 'content/teakdecks',
    create: true,
    slug: '{{title}}',
    editor: {
	preview: false
    },
    fields: [
	title,
	slug,
	draft,
	featuredImage,
	gallery,
	body
    ]
}

export default teakdecks
