import title from './field-title.js'
import slug from './field-slug.js'
import draft from './field-draft.js'
import body from './field-body.js'
import featuredImage from './field-featured-image.js'
import gallery from './field-gallery.js'
import index from './field-index.js'

const herstellungsprozess  = {
    format: 'toml-frontmatter',
    name: 'herstellungsprozess',
    label: 'Herstellungsprozess',
    label_singular: 'Herstellungsprozess',
    folder: 'content/herstellungsprozess',
    create: true,
    slug: '{{title}}',
    editor: {
	preview: false
    },
    fields: [
	title,
	slug,
	draft,
	index,
	gallery,
	body
    ]
}

export default herstellungsprozess
