const imageGalleries = Array.from(document.querySelectorAll('image-gallery'))

if (imageGalleries && imageGalleries.length) {
    imageGalleries.forEach(gallery => {
	var flkty = new Flickity(gallery, {
	    cellSelector: 'figure',
	    cellAlign: 'left',
	    contain: true,
	    pageDots: false,
	    fullscreen: true,
	    lazyLoad: 1
	})
    })
}

const cardGalleries = Array.from(document.querySelectorAll('card-gallery'))

if (cardGalleries && cardGalleries.length) {
    cardGalleries.forEach(gallery => {
	var flkty = new Flickity(gallery, {
	    cellSelector: '.Card',
	    cellAlign: 'left',
	    contain: true,
	    pageDots: false,
	    fullscreen: false,
	    lazyLoad: 1
	})
    })
}
