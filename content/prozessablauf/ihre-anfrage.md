+++
title = "Ihre Anfrage"
number = "1"
+++


Damit wir Sie bestmöglich zu Ihrer Anfrage beraten und Ihnen ein individuelles Angebot erstellen können, benötigen wir folgende Angaben:

* Bootstyp
* Baujahr
* Liegeplatz
* Bilder vom Deck
* wenn möglich: den **Decksplan** oder eine grobe Skizze der Decksflächen
* Ihre Rechnungsadresse



Senden Sie uns Ihre Anfrage an info@mobilerbootsbau.de