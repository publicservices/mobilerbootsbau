+++
title = "Montage"
number = "5"
+++


**Mit dem fertigen Deck kommen wir zu Ihnen — Europaweit.** Üblicherweise wird dabei das alte Teakdeck entfernt, der Untergrund aufgearbeitet und das neue Deck im Vakuumklebeverfahren montiert. Ihr Boot ist nach einer kurzen Montage-zeit wieder einsatzbereit!