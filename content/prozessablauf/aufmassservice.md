+++
title = "Aufmaßservice"
number = "3"
+++


**Wir kommen zu Ihnen an Bord** und erstellen dreidimensionale Schablonen von den Decks-flächen. Je nach Anwendung digital mit einem unserer computergestützten Messsysteme oder handwerklich aus dünnem, verzugsfreien Sperrholz.