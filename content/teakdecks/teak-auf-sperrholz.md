+++
title = "Teak Auf Sperrholz"
Slug = "teak-auf-sperrholz"
image = "media/uploads/Galerie-Startseite-P3128739.JPG"

[[gallery]]
image = "media/uploads/Sperrholz-P3118708.JPG"
caption = "sperrholz 1"

[[gallery]]
caption = "sperrholz 2"
image = "media/uploads/Sperrholz-P3118710.JPG"
+++
\
**Im modernen Holzbootsbau und bei der Restauration klassischer Yachten wird das Bootsdeck meist aus Bootsbausperrholz gefertigt und mit einem Stabdeck belegt. Eine sichere und bewährte Bauweise, wenn es der Feuchtigkeit unmöglich gemacht wird, in das Sperrholzdeck einzudringen.**

Der Bau von Booten aus Holz ist gleichbedeutend mit den Anfängen des Yachtbaus überhaupt — Holz als Bootsbaumaterial hat sich bis heute bewährt. Die Baumethoden haben sich jedoch weiterentwickelt und der moderne Holzbootsbau hat den traditionellen zum großen Teil abgelöst. Fast jede klassische Yacht trägt heute ein Teakdeck und gerade diesen Booten stehen die edlen Stabdecksbeläge besonders gut: Keine Decksbauweise verleiht einer Yacht mehr Klasse als ein geschwungenes Deck aus Teakholz.

Anstelle auf traditionelle Art kräftige Decksplanken direkt auf den Decksbalken zu befestigen wird heute  vermehrt Bootsbausperrholz als Deck und Träger für das Stabdeck verwendet. Auf Decksbalken, Balkweger und Schlingen wird eine Sperrholzlage befestigt, auf der das Teakdeck montiert wird. Allerdings ist diese Bauart anfällig gegen eindringende Feuchtigkeit. Hat das Wasser einmal seinen Weg zum Sperrholz gefunden, löst sich auch das beste Material binnen weniger Jahre auf.

Daher montieren wir ausschließlich moderne Teakdecks mit Wasserdampfsperre auf Sperrholzdecks — damit sie das Wasser unterm Kiel und nicht im Sperrholz haben.

![Auch hochwertigste Bootsbausperrhölzer sind anfällig gegen eindringende Feuchtigkeit](media/uploads/img_0346.png "Sperrholz Schaden durch Feuchtigkeit")

![Moderne Holzbauweise: Ein Deck wird aus Bootsbau-sperrholz auf Decksbalken gebaut. Auf diese Strukturdecks montieren wir wasserdampfdichte Teakdecks im Vakuumklebeverfahren.](media/uploads/pc054657.jpg "Moderne Holzbauweise")

![Ein Sperrholzdeck ohne und mit Wasserdampfsperre: Ohne Wasserdampfsperre dringt eindringende Feuchtigkeit in das Sperrholzdeck ein. Das Bootsbausperrholz nimmt das Wasser auf und zersetzt sich. Mit Wasserdampfsperre bleibt das Sperrholzdeck geschützt.](media/uploads/anwendung_sperrholz_vorhernachher.png "Wasserdampfsperre")