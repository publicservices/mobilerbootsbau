+++
title = "Stahlyachten"
Slug = "stahlyachten"
image = "media/uploads/Bilder-Stahl-IMG_8811.jpg"

[[gallery]]
image = "media/uploads/Bilder-Stahl-IMG_6274.jpg"
caption = "stahl 1"

[[gallery]]
caption = "stahl 2"
image = "media/uploads/Bilder-Stahl-IMG_0436.PNG"

[[gallery]]
caption = "stahl 3"
image = "media/uploads/Stahl-P1011653.JPG"

[[gallery]]
caption = "stahl 4"
image = "media/uploads/Bilder-Stahl-IMG_8738.jpg"
+++
Stahl ist seit jeher ein traditioneller Werkstoff im Schiffsbau und es werden noch heute hochwertige und individuelle Yachten aus diesem Material gefertigt. Es sind in der Vergangenheit vermehrt Fälle von Korrosion des Stahldecks unter dem Teak bekannt geworden.

Die Ursachen sind einfach zu benennen: Herkömmliche Decks und undichte Fugen sperren Feuchtigkeit ein, diese kann an den Stahl gelangen und führt zu Rost und Korrosionsproblemen. Häufig werden Sperrholzplatten mit dem Stahldeck verklebt und verschraubt um das Deck nach dem Schweißen zu nivellieren und um später die Teakstäbe besser montieren zu können. Es ist es eine Frage der Zeit, bis solche Decks Wasser im Sperrholz einschließen, das Holz sich zersetzt und die Korrosion des Stahldecks beginnt.

![Um das Teakdeck besser montieren zu können wird oft ein Bootsbausperrholz auf den Stahl geklebt: Eine Frage der Zeit, bis sich dieses zersetzt](media/uploads/img_0444.png "Sperrholz auf Stahl")

![Ohne Wasserdampfsperre besteht die Gefahr, dass der Stahl unter dem Teakdeck durch eindringende Feuchtigkeit korrodiert](media/uploads/img_0438.png "Schaden am Stahl durch Korrosion")

**Diese Schadensbilder sind vermeidbar.** Unsere modernen Teakdecks schützten den Stahl besser als es ein Farbanstrich oder Antirutschbelag zu leisten vermag. Die Schutzwerte unserer Teakdecks übertreffen bei weitem die moderner Korrosionsschutzanstriche — auch im Bezug auf Wasserdampfdichtigkeit und Oxidationsschutz. Zusätzlich schützt ein Teakdeck den Korrosionsschutz vor Witterungs- und UV-Einflüssen sowie vor mechanischer Beschädigung.



![Ein Stahldeck ohne und mit Wasserdampfsperre: Ohne Wasserdampfsperre kann eindringende Feuchtigkeit bis an das Stahldeck gelangen und Korrosion verursachen. Eine Wasserdampfsperre hindert eindrigende Feuchtigkeit daran, in den Stahl einzudringen. Das Deck bleibt geschützt.](media/uploads/anwendung_stahl_vorhernachher.png "Wasserdampfsperre")