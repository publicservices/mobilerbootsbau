+++
title = "Teakdecks"
nav_weight = 1
image = "media/uploads/soluna-1.jpg"

[[gallery]]
image = "media/uploads/Bilder-Stahl-IMG_8811.jpg"
caption = "Beispiel 1"

[[gallery]]
image = "media/uploads/Galerie-Startseite-P1011635.JPG"
caption = "Beispiel 2"

[[gallery]]
image = "media/uploads/GFK-IMG_0328.PNG"
caption = "Beispiel 3"

[[gallery]]
image = "media/uploads/Galerie-Startseite-P5100627.jpg"
caption = "Beispiel 4"
+++


Seit über 10 Jahren beschäftigen wir uns bei Mobilerbootsbau fast ausschließlich mit dem Bau und der Sanierung von Teakstabdecks. Wir haben es uns zur Aufgabe gemacht, das Teakdeck zu einem langlebigen und hochwertigen Produkt zu machen, mit alten und fest verankerten Bootsbautraditionen aufzuräumen und den Bau eines Stabdecks den Anforderungen und den Möglichkeiten des heutigen Yachtbaus anzupassen. Ich möchte Ihnen hier ein modernes Verfahren zeigen um langlebige, wartungsfreundliche und hochwertigste Decks zu bauen, welche nach der Montage das eigentliche Bootsdeck, sei es aus Stahl, Aluminium, GFK oder einem anderen Composit, zusätzlich schützen.

Ein Stabdeck zu bauen ist eine Kunst, die viele Bootsbaubetriebe beherrschen - die Meere sind voll von hochwertigen europäischen Yachten mit schönen und edlen Decksbelägen. Kunstvoll laufen Laibungen um Decksbeschläge und Winschen, werden Stabdecksleisten den Linien folgend eingebogen und laufen mit bis ins kleinste Detail ausgearbeiteten Butten in einem Fisch zusammen. Doch leider werden viel zu oft traditionelle Methoden angewendet, ohne dass die heutigen technischen Möglichkeiten ausgeschöpft werden. Es wird keine Rücksicht auf die Bauart und den Schutz der eigentlichen Yacht genommen. Ich denke hier an GFK-Sandwichdecks, die beim Bau des Teakdecks mit hunderten bis tausenden Bohrungen nachhaltig undicht gemacht werden und sich durch eindringendes Wasser delaminieren. Oder Stahlyachten, die unter dem Decksbelag rosten, weil das Teakholz direkt auf den beschichteten Stahl geklebt wurde. Sperrholzdecks, die sich zersetzen, weil undichte Decksfugen Feuchtigkeit zwischen Teak und Sperrholz einsperren. Oder Aluminiumdecks, die korrodiert sind, weil nicht bekannt war, dass die Inhaltsstoffe im Teakholz das Aluminium angreifen können. Es ist meist nicht die Schuld der Bootsbauer und Werften, denn diese Bauweisen stehen in den Büchern, werden in den Meisterkursen gelehrt und bereits seit vielen Jahrzehnten angewandt. Die Erbauer der traditionellen Decks bekommen die meist erst viele Jahre später auftretenden Probleme unter Umständen gar nicht mit, die Boote haben eventuell schon längst neue Eigentümer - die Spur verblasst, die Decks werden überarbeitet und das Ganze geht von vorne los.

Durch unseren täglichen Umgang mit diesen Problematiken und durch die Erkenntnisse, die wir bei den vielen Abrissen alter Teakdecks erlangt haben, konnten wir die Probleme und deren Ursachen verstehen. So konnten wir ein Produkt entwickeln, dass den vielseitigen Anforderungen im Yachtbau gerecht wird: das moderne Teakdeck.