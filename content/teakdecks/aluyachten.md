+++
title = "Aluyachten"
Slug = "aluyachten"
intro_text = "Welcome to Alu"
image = "media/uploads/Aluminium-IMG_6380.JPG"

[[gallery]]
image = "media/uploads/Aluminium-IMG_6380.JPG"
caption = "Aluminium caption"

[[gallery]]
image = "media/uploads/Bilder-Projektbeispiele-Teakdecks-IMG_8595.JPG"
+++
**Aluminium ist als Rumpfmaterial vor allem im individuellen Yachtbau zu Hause. Die Vorteile des Leichtmetalls werden sowohl von Eignern als auch von Werften geschätzt und viele dieser Einzelbauten werden mit Stabdecksbelägen ausgerüstet.**

Aluminium ist aus dem Boots- und Yachtbau heute nicht mehr wegzudenken. Von individuellen und hochpreisigen Einzelbauten bis zur großen Megayacht finden wir jeden Bootstyp in diesem Material. Eine Stahlyacht benötigt einen aufwändigen Korrosionsschutz, die Aluminiumlegierung dagegen verfügt über eine natürliche Korrosionsbeständigkeit. Der aktuelle Trend im Bootsbau verläuft zu Gunsten des Leichtmetalls.

Häufig schmücken edle Teakdecks diese hochpreisigen und individuellen Aliminiumyachten. Wenn die Besonderheiten dieses Leicht-metalls bereits während des Baus des Stabdecks berücksichtigt werden, haben die Eigner dieser Boote über Jahrzehnte hinweg ein sorgenfreies Deck.

![Aluminium Schaden](media/uploads/p5226006.jpg "Inhaltsstoffe im Holz können bei direktem Kontakt mit dem Leichtmetall und Feuchtigkeit ein Milieu bilden, das Korrosion fördert")

Bei Sauerstoffzutritt, also schon an der Luft, überzieht sich Aluminium mit einer Oxydschicht. Diese Schicht schützt den Werkstoff zuverlässig vor weiterer Korrosion. Allerdings konnten wir trotz dessen unter älteren, meist geschraubten Teakdecks auf Aluminiumyachten Korrosion feststellen — hervorgerufen durch eingedrungenes Wasser, welches sich zwischen Teak- und Aluminiumdeck sammelt und Salz, Verschmutzungen und einige Inhaltsstoffen im Teak löst. Dieses chemische Milieu macht es dem Aluminium schwer seine schützende Oxydschicht zu erneuern und es wird angreifbar.

Es ist demzufolge unerlässlich, durch bauliche Maßnahmen des Teakdecks zu verhindern, dass sich Feuchtigkeit zwischen Holz und Aluminium sammelt und die chemischen Prozesse beeinflusst, die für die Bildung der natürlichen Schutzschicht des Aluminiums verantwortlich sind.

Dies kann nur durch eine Wasserdampfsperre im Deck gewährleistet werden — ein Baumerkmal, dass alle unsere Teakdecks für Aluminiumyachten aufweisen.

![Aluminium mit Wasserdampfsperre](media/uploads/anwendung_aluminium_vorhernachher.png "Ohne Wasserdampfsperre dring Feuchtigkeit durch eine beschädigte Decksfuge bis an das Aluminiumdeck vor. Lochfraß und Aluminiumoxidation sind die Folgen. Mit Wasserdampfsperre wird eindringende Feuchtigkeit gestoppt, bevor sie das Deck erreichen kann. Das Aluminiumdeck bleibt geschützt.")