+++
title = "GFK Yachten"
Slug = "gfk-yachten"
image = "media/uploads/Bilder-GFK-IMG_3059.JPG"

[[gallery]]
image = "media/uploads/Bilder-GFK-IMG_0317.PNG"
caption = "gfk caption 1"

[[gallery]]
image = "media/uploads/Bilder-GFK-P5100627.jpg"
caption = "gfk caption 2"

[[gallery]]
image = "media/uploads/Bilder-GFK-IMG_0336.PNG"
caption = "gfk caption 3"
+++
**Die meisten Yachten und Boote sind heute aus Kunststoff gefertigt. GFK-Bauweisen haben den Bootsbau revolutioniert. Doch es gibt auch bei diesem als pflegeleicht geltenden Werkstoff Wichtiges zu beachten.**

Typischerweise verhilft ein hochwertiges Teakdeck einer Kunststoffyacht zum schiffigen auftreten, dessen sind sich nicht nur Qualitätshersteller Hallberg-Rassy, Najad, Oyster, Contest und viele andere bewusst. Sie rüsten ihre Neuboote schon ab Werft mit edlen Stabdecksbelegen aus.

Doch auch der als wartungsfreundlich und pflegeleicht geltende glasfaserverstärkte Kunststoff und die heute üblichen Sandwichdeckskonstruktionen sind anfällig gegen eindringende Feuchtigkeit 

> ![]()

und so manches unsachgemäß installiertes Teakdeck hat das Leben einer teuren Yacht schon vorzeitig beendet.

![GFK Problem Feuchtigkeit](media/uploads/Bilder-GFK-20151120_135749.jpg "Kernsanierung: Ein undichtes Deck hat Feuchtigkeit in dem Sandwichkern dieser Najad 34 eingeschlossen.")

![GFK Schaden Gelcoatbeschichtung](media/uploads/img_5984.jpg "Die Gelcoatbeschichtung dieser Kunststoffyacht hat durch eingeschlossene Feuchtigkeit schaden genommen und Risse gebildet.")

```

```

Herkömmlicherweise werden Decks auf Kunststoffyachten entweder durch die obere GFK-Lage des Sandwichdecks geschraubt oder es werden beim Decksbau vor Ort hunderte Löcher für Hilfskonstruktionen und Spannvorrichtungen in das vorher dichte Kunststoffdeck gebohrt. Kommt es nach Jahren einmal zu einer undichten Fuge findet das Wasser früher oder später seinen Weg in den Sandwichkern, dieser löst sich auf und verliert die Haftung zu den GFK-Lagen: Das Deck delaminiert sich.

Solche Schadensbilder sind leider häufig und kaum eine ältere Yacht mit einem geschraubten Deck weist nicht erhöhte Werte bei einer Feuchtigkeitsmessung des Sandwichdecks auf. Symptome die durch die Wahl der richtigen Decksbauweise vermeidbar sind.

Beim Bau und der Montage unserer modernen Teak-decks für Kunststoffyachten gehen wir gezielt auf diese Problematiken ein. Zum einen werden die Decks von uns ausschließlich auf vorbereitete und dichte GFK-Oberflächen geklebt, zum anderen haben unsere Decks bereits ab Werk eine Epoxydharz-Wasserdampfsperre eingebaut, welche eventuell eindringende Feuchtigkeit zuverlässig daran hindert, das eigentliche Bootsdeck zu erreichen.

Mit einem modernen Teakdeck können sie Ihre GFK-Yacht sorgenfrei genießen, denn Sie haben sich für ein Teakdeck ohne Nebenwirkungen entschieden.



![](media/uploads/anwendung_gfk_vorhernachher.png "GFK ohne / mit Wasserdampfsperre: Feuchtigkeit dringt ohne Wasserdampfsperre bis in das Laminat und in den Sandwichkern vor. Mit Wasserdampfsperre wird durch eine beschädigte Decksfuge eindringende Feuchtigkeit gestoppt und das Deck bleibt geschützt.")