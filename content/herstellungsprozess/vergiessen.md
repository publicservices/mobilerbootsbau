+++
title = "Vergießen"
number = "8"
image = ""
+++
Unsere modernen Teakdecks werden im Druck-verfahren mit silanmodifiziertem MS-Polymer vergossen. Dieses Material verbindet die Vorteile von Pulyurethan und Silikon und ist nicht nur in puncto UV-Beständigkeit das Beste, was der Markt zu bieten hat. In speziellen Trockenräumen erlauben wir dem Dichtstoff über mehrere Tage unter kontrollierten Bedingungen optimal auszuhärten.