+++
title = "Transport und Logistik"
number = "10"
image = ""
+++
Mit unseren Werkstattwägen transportieren wir unsere Teakdecks europaweit zu unseren Kunden. Unsere Bootsbauer reisen mit dieser mobilen Werkstatt samt Teakdeck an, ausgerüstet um auf alle Eventualitäten vor Ort flexibel reagieren zu können. Für den Transport sehr großer Decks oder unserer Teakdecks zur Selbstmontage arbeiten wir mit ausgewählten Speditionen zusammen.