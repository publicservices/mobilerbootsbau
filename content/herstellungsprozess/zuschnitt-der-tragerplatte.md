+++
title = "Zuschnitt der Trägerplatte"
number = "2"
image = ""
+++


Die 1 mm starke und dampfsperrende Epoxydharz-Glasfaserplatte wird der Decksform entsprechend zugeschnitten und auf einem unserer speziellen Decksbautische für die Verklebung vorbereitet.