+++
title = "Leisten und Laibungen"
number = "3"
image = ""
+++
Der Zuschnitt der einzelnen Stabdecksleisten und Laibungen erfolgt aus handverlesenen Bohlen. Wir legen großen Wert darauf, die gesamte Konfektion des Teakholzes vom Stamm bis zur fertigen Leiste bei uns im Haus und unter strengen Qualitätskontrollen durchzuführen - Ein Deck kann nur so gut sein wie das Material, aus dem es gebaut ist.