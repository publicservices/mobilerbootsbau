+++
title = "Fügen"
number = "5"
image = ""
+++


Auf den speziellen Decksspanntischen nimmt das Teakdeck nun erstmalig seine Form an. Die einzelnen Bauteile werden eingespannt, über der Epoxydharz-trägerplatte eingebogen und positioniert. Die Trägerplatte wird ebenfalls mit speziellen chemischen Haftvermittlern für die optimale Verklebung vorbereitet, der dauerelastische Klebstoff wird appliziert und die Deckselemente werden zusammengefügt.