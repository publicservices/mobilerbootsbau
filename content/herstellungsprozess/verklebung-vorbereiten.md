+++
title = "Vorbereiten der Verklebung"
number = "4"
image = ""
+++


Sobald das Material konfektioniert ist, beginnen die technisch-chemischen Prozesse. Verklebungen, die Jahrzehnten der Witterung trotzen sollen, bedürfen besten Materialien und gründlichster Untergrundvorbereitung. Neben der mechanischen Vorbereitung der Oberflächen durch feines Strahlen und Schleifen kommen in diesem Prozessabschnitt spezielle Lösemittel und Haftvermittler zum Einsatz, um die bestmöglichen Verklebungen zu erreichen.