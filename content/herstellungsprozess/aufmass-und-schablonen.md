+++
title = "Aufmaß und Schablonen"
number = "1"
image = "media/uploads/Aluminium-IMG_6380.JPG"
+++


Für den kundenspezifischen Bau der Decks beginnen wir mit dem dreidimensionalen millimetergenauen Aufmaß der Decksflächen auf der Yacht. Es werden hierfür je nach Anwendung präzise Schablonen gefertigt oder das Deck mit einem digitalen Mess-system vermessen. Ob handwerklicher Schablonenbau oder digitales Aufmaß: in beiden Fällen wird das neue Deckslayout anschließend computergestützt entworfen.