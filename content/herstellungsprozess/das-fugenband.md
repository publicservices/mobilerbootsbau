+++
title = "Das Fugenband"
number = "7"
image = ""
+++
Die Dreiflankenhaftung: Teakholz besitzt zwar ein vergleichsweise geringes Quell- und Schwund-verhalten, kann unter wechselnden klimatischen Bedingungen aber geringfügig arbeiten. Die Teakdecksfugen müssen diese Stauch- und Dehnungskräfte aufnehmen können, ohne sich von der Fugenflanke zu lösen. Deshalb ist es erforderlich, die Dreiflankenhaftung des Dichtstoffes zu verhindern. Aus diesem Grund legen wir nach dem Ablüften des Primers stets ein spezielles Fugenband in alle Decksnähte ein.