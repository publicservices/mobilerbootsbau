+++
title = "Vorbehandlung"
number = "6"
image = ""
+++


Mehrere Tage dauert es, bis die chemischen Prozesse der Verklebung abgeschlossen sind und das Deck für das Verfugen der Decksnähte vorbereitet werden kann. Um die besten Ergebnisse zu erzielen, müssen alle Decksfugen in mehreren Arbeitsgängen angeraut, gereinigt und mit einem speziellen Haftvermittler beschichtet werden. Erst dann folgt einer der wichtigsten und lebensverlängernden Schritte: Das Einlegen eines Fugenbands.